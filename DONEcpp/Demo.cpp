#include "DONE.h"
#include "cblas.h"
#include "ziggurat.h"
#include <iostream>

static Ziggurat::Ziggurat zigg2; //Random number generator


double * TestFunc(double *dataset, int dim, void * extra_parameters) {
	//Gaussian-shaped test function for noisy function minimization
	dataset[dim] = 0;
	for (int jjj = 0; jjj < dim; jjj++) {
		dataset[dim] += (dataset[jjj] - 0.3)* (dataset[jjj] - 0.3);
	}
	dataset[dim] = -exp(-dataset[dim]) - 3;
	dataset[dim] += 0.01*zigg2.norm();
	std::cout << "\n" << dataset[dim];
	return dataset;
}

int main() {
	std::cout << "Start optimization of f(x)=-exp(||x-0.3||^2)-3 + random noise with the DONE algorithm.\n\n";

	const int d = 2; // dimension of input function
	int N = 200; // total number of measurements
	int D = 500; // dimension of random Fourier expansion
	double sigma = 1.0; // kernel bandwidth 
	double lambda = 1e-6; // regularization parameter
	double expl = 1e-2; // perturbation for next measurement
	double *x0; // initial guess
	double lb = -1; // lower bound
	double ub = 1; // upper bound
	x0 = new double[d];
	//zigg2.setSeed(123456789);
	zigg2.setSeed(time(NULL));
	for (int jjj = 0; jjj < d; jjj++) {
		x0[jjj] = 1.0*zigg2.norm();
	}
	double *xopt;
	//Run the DONE algorithm
	xopt = DONE([&](double *xx) {return TestFunc(xx, d, 0); }, x0, d, N, lb, ub, D, lambda, sigma, expl);

	//Show results
	std::cout << "\n\nFound optimum:\n\nx=\n";
	for (int jjj = 0; jjj < d; jjj++) {
		std::cout << xopt[jjj] << "\n";
	}

	getchar();
	return 0;
}
