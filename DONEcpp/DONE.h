#ifndef DONE_H
#define DONE_H

#include <math.h> 
#include <iostream>
#include <random>
#include <ctime>	
#include "cblas.h"
#include "dlib/optimization.h"
#include "ziggurat.h"
#include <exception>
#define DLIB_USE_BLAS
//#pragma
typedef dlib::matrix<double, 0, 1> column_vector;
static Ziggurat::Ziggurat zigg;
double pi = 3.141592653589793238462643383279502884;




void cppblas_drothg(double * da, double * db, double * c, double * s) {
	double r = 0, roe, scale, z;
	//INTRINSIC dabs, dsign, dsqrt      
	roe = (*db);
	if (abs(*da) > abs(*db)) {
		roe = *da;
	}
	scale = abs(*da) + abs(*db);
	if (scale == 0.0) {
		(*c) = 1.0;
		(*s) = 0.0;
		r = 0.0;
		z = 0.0;
	}
	else {
		if (abs(*da) > abs(*db)) {
			r = scale*sqrt(pow(((*da) / scale), 2) - pow(((*db) / scale), 2));
			r = ((double)(roe > 0) - (double)(roe < 0))*r;
			(*c) = (*da) / r;
			(*s) = (*db) / r;
		}
		else if (abs(*db) > abs(*da)) {
			std::cout << "WARNING: BAD iQR downdate, P is not positive definite, this isn't even possible \n";
			r = scale*sqrt(pow(((*db) / scale), 2) - pow(((*da) / scale), 2));
			r = ((double)(roe > 0) - (double)(roe < 0))*r;
			(*c) = (*db) / r;
			(*s) = (*da) / r;

		}
		z = 1.0;
		if (abs(*da) > abs(*db)) { z = *s; }
		if (abs(*db) >= abs(*da) && (*c != 0.0)) { z = 1.0 / (*c); }
	}
	(*da) = r;
	(*db) = z;
}

void cppblas_droth(const int n, double *dx, const int incx, double *dy, const int incy, const double c, const double s) {
	double dtemp;
	int ix, iy;
	if (n <= 0) { return; }
	if (incx == 1 && incy == 1) {
		for (int i = 0; i < n; i++) {
			dtemp = c*dx[i] - s*dy[i];
			dy[i] = c*dy[i] - s*dx[i];
			dx[i] = dtemp;
		}
	}
	else {
		ix = 0;
		iy = 0;
		if (incx < 0) { ix = (-n + 1)*incx; }
		if (incy < 0) { iy = (-n + 1)*incy; }
		for (int iii = 0; iii < n; iii++) {
			dtemp = c*dx[ix] + s*dy[iy];
			dy[iy] = c*dy[iy] - s*dx[ix];
			dx[ix] = dtemp;
			ix = ix + incx;
			iy = iy + incy;
		}
	}
}


void cppblas_drotg(double * da, double * db, double * c, double * s) {
	double r, roe, scale, z;
	//INTRINSIC dabs, dsign, dsqrt      
	roe = (*db);
	if (abs(*da) > abs(*db)) {
		roe = *da;
	}
	scale = abs(*da) + abs(*db);
	if (scale == 0.0) {
		(*c) = 1.0;
		(*s) = 0.0;
		r = 0.0;
		z = 0.0;
	}
	else {
		r = scale*sqrt(pow(((*da) / scale), 2) + pow(((*db) / scale), 2));
		r = ((double)(roe > 0) - (double)(roe < 0))*r;
		(*c) = (*da) / r;
		(*s) = (*db) / r;
		z = 1.0;
		if (abs(*da) > abs(*db)) { z = *s; }
		if (abs(*db) >= abs(*da) && (*c != 0.0)) { z = 1.0 / (*c); }
	}
	(*da) = r;
	(*db) = z;
}

void cppblas_drot(const int n, double *dx, const int incx, double *dy, const int incy, const double c, const double s) {
	double  dtemp;
	int ix, iy;

	if (n <= 0) { return; }
	if (incx == 1 && incy == 1) {
		//dtemp = 1 / c;
		for (int i = 0; i < n; i++) {
			dtemp = c*dx[i] + s*dy[i];
			dy[i] = c*dy[i] - s*dx[i];
			dx[i] = dtemp;
		}
	}
	else {
		ix = 0;
		iy = 0;
		if (incx < 0) { ix = (-n + 1)*incx; }
		if (incy < 0) { iy = (-n + 1)*incy; }
		for (int iii = 0; iii < n; iii++) {
			dtemp = c*dx[ix] + s*dy[iy];
			dy[iy] = c*dy[iy] - s*dx[ix];
			dx[ix] = dtemp;
			ix = ix + incx;
			iy = iy + incy;
		}
	}
}






void DispMat(double *aMatrix, int Row, int Col) {
	/* Displays elements in array aMatrix into nr Row and Cols
	Author: Hans Verstraete */
	for (int row = 0; row < Row; row++) {
		for (int col = 0; col < Col; col++) {
			std::cout << aMatrix[row*Col + col] << "  ";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}

void MultVect(double *aVector, double *bVector, double *cVector, int len) {
	/* Elementwise multiplication of Vector
	Author: Hans Verstraete */
	for (int row = 0; row < (len); row++) {
		//Add two vectors of the defined length 
		cVector[row] = aVector[row] * bVector[row];
	}
}

double MaxAbsVec(double *aMatrix, __int64 len) {
	double temp = -INFINITY;
	for (__int64 jjj = 0; jjj < len; jjj++) {
		if (abs(aMatrix[jjj]) > temp) {
			temp = abs(aMatrix[jjj]);
		}
	}
	return temp;
}

double MinAbsVec(double *aMatrix, __int64 len) {
	double temp = INFINITY;
	for (__int64 jjj = 0; jjj < len; jjj++) {
		if (abs(aMatrix[jjj]) < temp) {
			temp = abs(aMatrix[jjj]);
		}
	}
	return temp;
}


void PackedDiag(double *aMatrix, double scalar, int len) {
	/* Adds scalar value to the diagonal of packed aMAtrix with dimension len-by-len
	Author: Hans Verstraete */
	__int64 temp = 0;
	for (int jjj = 0; jjj < (len); jjj++) {
		aMatrix[temp] = aMatrix[temp] + scalar;
		temp += (len - jjj);
	}
}

void NormalMatZig(double *aMatrix, double stdev, int Row, int Col) {
	/* Fills up values in array aMatrix length Row*Col with normal distributed values with std, stdev
	Author: Hans Verstraete */
	__int64 temp = Row;
	temp *= Col;
	for (__int64 row = 0; row < temp; row++) {
		//Create Normal distributed elements in defined matrix
		aMatrix[row] = stdev*zigg.norm();
	}
}


void UniformMat(double *aMatrix, int Row, int Col) {
	/* Fills up values in array aMatrix length Row*Col with uniform distributed values between 0 and 2*pi
	Author: Hans Verstraete */
	std::default_random_engine generator;
	std::uniform_real_distribution<double> unidistribution(0.0, 2 * pi);
	__int64 temp = Row;
	temp *= Col;
	for (__int64 row = 0; row < temp; row++) {
		//Create Uniform distributed elements in defined matrix
		aMatrix[row] = unidistribution(generator);
	}
}

void CosMat(double *aMatrix, double *CosMatrix, int Row, int Col) {
	/* Calculates the cosine of values in array aMatrix with length Row*col and stores it in CosMatrix
	Author: Hans Verstraete */
	for (int row = 0; row < (Row*Col); row++) {
		//Take the cosine of every element in the defined matrix
		CosMatrix[row] = cos(aMatrix[row]);
	}
}

void SinMat(double *aMatrix, double *SinMatrix, int Row, int Col) {
	/* Calculates the sine of values in array aMatrix with length Row*col and stores it in SinMatrix
	Author: Hans Verstraete */
	for (int row = 0; row < (Row*Col); row++) {
		//Take the cosine of every element in the defined matrix
		SinMatrix[row] = sin(aMatrix[row]);
	}
}



void iQRupdate(int N, double *P, double *U, double *G, const double *Y, double *W, double *C, double *S) {
	/*Purpose
	The problem is
	(1      U*P)                          (gamma^(-1/2)          0)
	(              )   THETA'  =   (                                     ) ,
	(0       P)                     (g*gamma^(-1/2)'      P2)
	Where THETA is a product of Givens plane rotations.
	When P, W are the factors of a least square problem,
	this is equivalent to adding a new observation(U, Y),
	and update the solution P, W of the problem.

	N(input) INTEGER
	The order of the matrix R.N >= 0.

	P(input / output) PACKED DOUBLE PRECISION array, dimension(N, N)
	On entry, the lower triangular matrix P. The leading N - by - N lower triangular part of P contains the lower
	triangular part of the matrix P. On exit, P contains the updated Cholesky factor

	U(input / output) DOUBLE PRECISION array, dimension(N)
	On entry, U contains the vector U used to update P.
	On exit, U is destroyed.

	G(input / output) DOUBLE PRECISION array
	G is considered to be dimension(N) (input values ignored)
	On exit, G contains the updated matrix G

	Y(input / output) DOUBLE PRECISION array, dimension(1)

	W(input / output) DOUBLE PRECISION array, dimension(N)
	on entry, W contains the weights (solution) of the least squares problem.
	on exit, W contains the updated weights (solution) of the least squares problem.

	C(output),S(output) DOUBLE PRECISION array, dimension(N)
	C,S contains the cosines,sines of performed Givens rotations.

	Authors: Hans Verstraete & Laurens Bliek*/
	memset(G, 0, N*sizeof(double));
	double GAMMA = 1;
	double dtemp = cblas_ddot(N, U, 1, W, 1);
	cblas_dtpmv(CblasColMajor, CblasLower, CblasTrans, CblasNonUnit, N, P, U, 1);        //THIS IS TEMPORARILY U = U*P
	//*     Quick return if possible
	if (N == 0) {
		return;
	}

	//*       Compute givens rotations to annihilate x
	__int64 temp = N;
	temp = (temp*temp + temp) / 2 - 1;
	for (int I = 0; I < N; I++) {
		cblas_drotg(&GAMMA, &U[N - I - 1], &C[I], &S[I]);
		//*         Force positive diagonal values
		if (GAMMA < 0.0) {
			GAMMA = -GAMMA;
			C[I] = -C[I];
			S[I] = -S[I];
		}
		//*         Apply rotation on each column
		cblas_drot(I + 1, &G[N - I - 1], 1, &P[temp], 1, C[I], S[I]);
		temp = temp - I - 2;
	}
	
	dtemp = (Y[0] - dtemp);

	cblas_dscal(N, 1 / GAMMA, G, 1);
	cblas_daxpy(N, dtemp, G, 1, W, 1);
	memset(U, 0, N*sizeof(double));
}





void iQRdowndate(int N, double *P, double *U, double *G, const double *Y, double *W, double *C, double *S) {
	/*Purpose

	ACCURATE DOWNDATING OF LEAST SQUARES SOLUTIONS*
	A. BJORCK, H. PARK, AND L. ELDEN

	The problem is
	(1      U*P)                      (gamma^(-1/2)          0)
	(                )   THETA'  =   (                                     ) ,
	(0       P)                       (g*gamma^(-1/2)'      P2)
	Where THETA is a product of HYPERBOLIC Givens plane rotations.
	When P, W are the factors of a least square problem,
	this is equivalent to adding a new observation(U, Y),
	and update the solution P, W of the problem.

	N(input) INTEGER
	The order of the matrix R.N >= 0.

	n(input) INTEGER
	The current iteration of the DONE algorithm

	P(input / output) PACKED DOUBLE PRECISION array, dimension(N, N)
	On entry, the lower triangular matrix P. The leading N - by - N lower triangular part of P contains the lower
	triangular part of the matrix P. On exit, P contains the updated Cholesky factor

	U(input / output) DOUBLE PRECISION array, dimension(N)
	On entry, U contains the vector U used to update P.
	On exit, U is destroyed.

	G(input / output) DOUBLE PRECISION array
	G is considered to be dimension(N) (input values ignored)
	On exit, G contains the updated matrix G

	Y(input / output) DOUBLE PRECISION array, dimension(1)

	W(input / output) DOUBLE PRECISION array, dimension(N)
	on entry, W contains the weights (solution) of the least squares problem.
	on exit, W contains the updated weights (solution) of the least squares problem.

	C(output),S(output) DOUBLE PRECISION array, dimension(N)
	C,S contains the cosines,sines of performed Givens rotations.

	Authors: Hans Verstraete & Laurens Bliek*/

	memset(G, 0, N*sizeof(double));
	double GAMMA = 1;
	double dtemp = cblas_ddot(N, U, 1, W, 1);
	cblas_dtpmv(CblasColMajor, CblasLower, CblasTrans, CblasNonUnit, N, P, U, 1);       //THIS IS TEMPORARILY U = U*P
																						//*     Quick return if possible
	if (N == 0) {
		return;
	}

	//*       Compute givens rotations to annihilate x
	__int64 temp = N;
	temp = (temp*temp + temp) / 2 - 1;
	for (int I = 0; I < N; I++) {
		cppblas_drothg(&GAMMA, &U[N - I - 1], &C[I], &S[I]);
		//*         Force positive diagonal values
		if (GAMMA < 0.0) {
			GAMMA = -GAMMA;
			C[I] = -C[I];
			S[I] = -S[I];
		}
		//*         Apply rotation on each column
		cppblas_droth(I + 1, &G[N - I - 1], 1, &P[temp], 1, C[I], S[I]);
		temp = temp - I - 2;
	}

	dtemp = (Y[0] - dtemp);
	cblas_dscal(N, 1 / GAMMA, G, 1);
	cblas_daxpy(N, dtemp, G, 1, W, 1);
	memset(U, 0, N*sizeof(double));
}


struct RFmodel {
	/* Structure for holding a full Random Fourier features model according Rahimi & Benjamin Recht
	Some parameters are to be used with the DONE algorithm
	Contains both an RLS updater and inverse QR RLS updater (the latter should be numerically more stable).
	Needs to be initialized with Init() or InitQR() AFTER initializing variables in the first Block.
	Authors: Hans Verstraete & Laurens Bliek*/

public:
	//all the variables that need to be assigned before calling Init() or InitiQR() are listed in this public
	//Random Fourier Features parameters
	int d;      //dimension input function
	int D;       // dimension fourier basis, number of random cosines
	int N;       // Maximum number of measurements
	double sigma; // Kernel bandwidth
	double lambda; // Regularization parameter
				   //DONE parameters
	double *lb, *ub; // lower and upperbound for optimization
	int explstrategy;
	double sigma1; // Perturbation for next measurement
	double sigmaend; // Final Perturbation for next measurement
	double offset;
	double PotMinF;
	double *PotMinArg; //This array will hold the potentially minimizing argument and verify when exploration is lowered.
	int windowsize;
	double * window;

private:
	double *Omega;
	double *b;
	double *w;
	double *ATAi;
	double *zx;
	double evalx;
	double *history;
	double *G;
	double *pWS1;
	double *pWS12;

public:
	int ItNr; //keeps track of the number of measurements that are added 
	__int64 MaxArrayLen;
	const void Zx(const double *x) {
		cblas_dcopy(D, b, 1, zx, 1);
		cblas_dgemv(CblasColMajor, CblasNoTrans, D, d, 1, Omega, D, x, 1, 1, zx, 1);
		CosMat(zx, zx, D, 1);
	}
	const double Evalx(const double *x) {
		Zx(x);
		evalx = cblas_ddot(D, w, 1, zx, 1);
		return evalx;
	}

	double operator()(const column_vector& x) const
	{
		cblas_dcopy(D, b, 1, zx, 1);
		cblas_dgemv(CblasColMajor, CblasNoTrans, D, d, 1, Omega, D, &x(0), 1, 1, zx, 1);
		CosMat(zx, zx, D, 1);
		return cblas_ddot(D, w, 1, zx, 1);
	}

	const column_vector Derx(const column_vector& x)      const
	{
		column_vector res(d);
		cblas_dcopy(D, b, 1, zx, 1);
		cblas_dgemv(CblasColMajor, CblasNoTrans, D, d, 1, Omega, D, &x(0), 1, 1, zx, 1);
		SinMat(zx, zx, D, 1);
		MultVect(w, zx, pWS1, D);
		cblas_dgemv(CblasColMajor, CblasTrans, D, d, -1, Omega, D, pWS1, 1, 0, &res(0), 1);
		return res;
	}
	
	const void InitiQR() {
		//Recursive Least Squares update.
		MaxArrayLen = D;
		MaxArrayLen = (MaxArrayLen*MaxArrayLen + MaxArrayLen) / 2;
		try {
			Omega = new double[D*d];    // initialization memory 
			b = new double[D];
			w = new double[D];
			ATAi = new double[MaxArrayLen];
			zx = new double[D];
			history = new double[D];
			G = new double[D];
			pWS1 = new double[D];
			pWS12 = new double[D];
			PotMinArg = new double[d];
			if (windowsize > 0) {
				window = new double[windowsize*(d + 1)];
				memset(window, 0, windowsize*sizeof(double));
			}
			PotMinF = INFINITY;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << '\n';
		}
		memset(w, 0, D*sizeof(double));
		memset(history, 0, D*sizeof(double));
		offset = 0;
		ItNr = 0;
		//Initializing omega and b
		NormalMatZig(Omega, sigma, D, d);
		UniformMat(b, D, 1);
		// Steps for the first ATAi : ATAi = I/lambda-Z*Z^T/(lambda^2+Z^T*Z*lambda);
		memset(ATAi, 0, MaxArrayLen*sizeof(double));
		PackedDiag(ATAi, 1 / sqrt(lambda), D);
	}

	const void UpdateiQR(const double *dataset) {
		//Recursive Least Squares update.
		double temp, temp2;
		ItNr = ItNr + 1;
		Zx(dataset);
		//update W using inverse QR
		if (offset + dataset[d] > 0) {
			//Changing the offset and weight with new offset here 
			temp = (-2 * dataset[d] - offset);
			PotMinF = PotMinF + temp;
			cblas_daxpy(D, temp, history, 1, w, 1);
			offset = -2.0 * dataset[d];
			//std::cout << "\nNew offset: " << offset << "\n";
		}
		//applying new measurement to update weights
		temp = dataset[d] + offset;
		///cblas_daxpy(D, 1, zx, 1, history, 1);
		temp2 = 1 - cblas_ddot(D, zx, 1, history, 1);
		iQRupdate(D, ATAi, zx, G, &temp, w, pWS1, pWS12);
		cblas_daxpy(D, temp2, G, 1, history, 1);
	}

	const void DowndateiQR(const double *dataset) {
		//Recursive Least Squares update.
		double temp, temp2;
		Zx(dataset);
		//downdate W using inverse QR
		temp = dataset[d] + offset;
		///cblas_daxpy(D, -1, zx, 1, history, 1);
		temp2 = 1 - cblas_ddot(D, zx, 1, history, 1);
		iQRdowndate(D, ATAi, zx, G, &temp, w, pWS1, pWS12);
		cblas_daxpy(D, temp2, G, 1, history, 1);
	}
};


template<typename func>
double * DONE(func& objfun, double * x0, int d, int N, double * lb, double * ub, double D, double lambda, double sigma, double expl, int windowsize) {
	/* Runs the DONE algorithm
	Authors: Hans Verstraete & Laurens Bliek*/
	int showtime = 0;
	std::clock_t start;
	double duration;

	//store settings in model 
	int windowtemp = 0;
	RFmodel model1;
	model1.d = d; //dimension of input function
	model1.N = N; //total number of measurements
	model1.D = D; //dimension of random Fourier expansion
	model1.sigma = sigma; //kernel bandwidth 
	model1.lambda = lambda; // regularization parameter
	model1.lb = lb; // lower bound
	model1.ub = ub; // upper bound
	model1.sigma1 = expl; // perturbation for next measurement
	model1.sigmaend = model1.sigma1 / 10;
	model1.explstrategy = 0; // 0 is fixed exploration, 1 is decrease after 80% and 90%, 2 is linear decrease
	model1.windowsize = windowsize;
	column_vector LB(model1.d);
	column_vector UB(model1.d);
	cblas_dcopy(model1.d, model1.lb, 1, &LB(0), 1);
	cblas_dcopy(model1.d, model1.ub, 1, &UB(0), 1);
	column_vector dataopt(model1.d);     //will be used as input and output coordinates of the optimization routine
	column_vector datanew(model1.d + 1);   //will hold the coordinates of the new measurement and measurement value
	model1.InitiQR();
	double CurMinF;
	cblas_dcopy(model1.d, x0, 1, &datanew(0), 1);
	cblas_dcopy(model1.d, x0, 1, model1.PotMinArg, 1);


	for (int jjj = 0; jjj < (model1.N); jjj++) {
		//New Measurement
		objfun(&datanew(0));
		//Update window of measurements
		if (model1.windowsize > 0) {
			windowtemp = jjj % model1.windowsize;
			if (jjj > (model1.windowsize - 1)) {
				model1.DowndateiQR(&(model1.window[windowtemp *(model1.d + 1)]));
			}
			memcpy(&(model1.window[windowtemp *(model1.d + 1)]), &datanew(0), sizeof(double)*(model1.d + 1));
		}
		//Update Model
		start = std::clock();
		model1.UpdateiQR(&datanew(0));
		//Run Optimization on obtained function with perturbation to starting point.
		NormalMatZig(&dataopt(0), model1.sigma1, model1.d, 1);
		//memset(&dataopt(0), 0, model1.d);
		cblas_daxpy(model1.d, 1, &datanew(0), 1, &dataopt(0), 1);
		dataopt = clamp(dataopt, LB, UB);
		CurMinF = find_min_box_constrained(dlib::lbfgs_search_strategy(10), dlib::objective_delta_stop_strategy(1e-9, 8), model1, [&](column_vector xx) {return model1.Derx(xx); }, dataopt, LB, UB);  // can change to smaller trust region  LB =  clamp(dataopt -model1.sigma1, LB, dataopt -model1.sigma1), UB = clamp(dataopt +model1.sigma1, dataopt +model1.sigma1, UB))
		//objfun(&dataopt(0));																																														   //check if the current Min is lower than the best min so far update on new model
		model1.PotMinF = model1.Evalx(model1.PotMinArg);
		if (CurMinF <= model1.PotMinF) {
			cblas_dcopy(model1.d, &dataopt(0), 1, model1.PotMinArg, 1);
			model1.PotMinF = CurMinF;
		}
		else {
			cblas_dcopy(model1.d, model1.PotMinArg, 1, &dataopt(0), 1);
		}

		// Update exploration parameter sigma1
		if (model1.explstrategy == 1) {
			if (jjj == round(0.8*model1.N)) {
				model1.sigma1 = (model1.sigma1 + model1.sigmaend) / 2;
			}
			else if (jjj == round(0.9*model1.N)) {
				model1.sigma1 = (model1.sigma1 + model1.sigmaend) / 2;
			}
			else if (jjj == round(0.95*model1.N)) {
				model1.sigma1 = model1.sigmaend;
			}
		}
		//Linear decrease in exploration parameter                  
		else if (model1.explstrategy == 2) {
			model1.sigma1 += (model1.sigmaend - model1.sigma1) / (double)model1.N;
		}

		// Define Next Measurement Coordinates
		if (jjj < (model1.N - 1)) {
			NormalMatZig(&datanew(0), model1.sigma1, model1.d, 1);
			cblas_daxpy(model1.d, 1, &dataopt(0), 1, &datanew(0), 1);
		}
		else {
			NormalMatZig(&datanew(0), 0, model1.d, 1);
			cblas_daxpy(model1.d, 1, &dataopt(0), 1, &datanew(0), 1);
		}

		

		datanew = clamp(datanew, LB, UB);
		//Show the iteration time of the DONE Algorithm if showtime = 1
		if (showtime == 1) {
			duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
			std::cout << "Duration Iteration " << jjj << " : " << duration << "  seconds \n";
		}

	}
	cblas_dcopy(model1.d, model1.PotMinArg, 1, x0, 1);

	return x0;
}


//DONE with scalar lower and upper bound
template<typename func>
double * DONE(func& objfun, double * x0, int d, int N, double lb, double ub, double D, double lambda, double sigma, double expl, int windowsize) {
	double *LB, *UB;
	LB = new double[d];
	UB = new double[d];
	for (int i = 0; i < d; i++) {
		LB[i] = lb;
		UB[i] = ub;
	}
	return DONE(objfun, x0, d, N, LB, UB, D, lambda, sigma, expl, windowsize);
}


//DONE with scalar lower and upper bound, no windowsize
template<typename func>
double * DONE(func& objfun, double * x0, int d, int N, double lb, double ub, double D, double lambda, double sigma, double expl) {
	double *LB, *UB;
	LB = new double[d];
	UB = new double[d];
	for (int i = 0; i < d; i++) {
		LB[i] = lb;
		UB[i] = ub;
	}
	return DONE(objfun, x0, d, N, LB, UB, D, lambda, sigma, expl, -1);
}

//DONE algorithm for noisy function minimization
template<typename func>
double * DONE(func& objfun, double * x0, int d, int N, double * lb, double * ub, double D, double lambda, double sigma, double expl) {
	return DONE(objfun, x0, d, N, lb, ub, D, lambda, sigma, expl, -1);
}



#endif