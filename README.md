# The DONE algorithm#
## Minimizing functions perturbed by noise##

The DONE algorithm is an optimization algorithm that is specifically designed for finding the minimum of functions that:

* are expensive to evaluate
* do not have derivatives available
* can only be accessed with noisy measurements

Examples are the tuning of parameters for some simulation, or hyper-parameter optimization problems.

DONE uses the available measurements to build a surrogate model, and the minimum of the surrogate model is found using standard optimization techniques. The surrogate model is a random Fourier expansion with a fixed number of cosine basis functions.

The main paper can be found here: http://ieeexplore.ieee.org/document/7728083/

This repository contains C++ source code of the algorithm with dependencies for 64-bit Windows systems. Other implementations can be found at:

* Julia: https://github.com/rdoelman/DONEs.jl
* Python wrapper: https://bitbucket.org/csi-dcsc/pydonec/src/master/
* Matlab: https://bitbucket.org/csi-dcsc/done_matlab/src/master/

Please contact l.bliek@tudelft.nl if you have any questions.

### Running the algorithm ###

To test the algorithm, download the source and run DONEcpp/Demo.exe to find the minimum of a 2-dimensional Gaussian.

To compile, open the C++ project and compile as is.

The folder contains the following files:

* DONEcpp.vcxproj: main project file
* DONE.h: DONE source code
* Demo.cpp: Demo file that minimizes a 2-dimensional Gaussian.
* ziggurat.h: Random number generator
* Openblas files to perform linear algebra computations
* dlib files to perform standard optimization procedures

The function DONE is called as follows:

```
#!c++

xopt = DONE([&](double *xx) {return TestFunc(xx, d, 0); }, x0, d, N, lb, ub, D, lambda, sigma, expl);
```
Here, xopt is the found optimum, TestFunc is the function to be minimized, x0 is the initial guess, d is the dimension of x0, N is the total number of measurements, lb and ub are the lower and upper bounds (either scalars or vectors), D is the number of basis functions of the surrogate function, lambda is the regularization parameter, sigma is the standard deviation of the frequencies of the cosines, and expl is the exploration parameter.

### Dependencies ###
This implementation uses LAPACK, OPENBLAS, and ZIGGURAT. The corresponding 64-bit Windows dlls are included in the package.
Openblas copyright: Copyright 2009, 2010 The University of Texas at Austin.
Lapack copyright: Copyright (c) 2010, Intel Corp.
Ziggurat copyright: Marsaglia + Tsang / Leong, Zhang et al Ziggurat generator, Copyright (C) 2013 Dirk Eddelbuettel